//更新用户信息
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database() //数据库
const updateUser = async function (params) {
  const wxContext = cloud.getWXContext()
  const updateResult = await db.collection('user').where({
    _openid: wxContext.OPENID
  }).update({
    data: params
  })
  if (updateResult.stats) {
    //更新成功
    const userResult = await db.collection('user').where({
      _openid: wxContext.OPENID
    }).get()
    return userResult.data[0]
  }
}
module.exports = updateUser