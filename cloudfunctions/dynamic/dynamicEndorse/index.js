//动态点赞接口
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database() //数据库

const dynamicEndorse = async function (params) {
  const wxContext = cloud.getWXContext()
  let endorseResult = null
  if (params.active === 1) {
    const isData = await db.collection("dynamicEndorse").where({
      _openid: wxContext.OPENID,
      dynamicId: params.dynamicId
    }).get()
    if (isData.data.length === 0) {
      endorseResult = await db.collection("dynamicEndorse").add({
        data: {
          _openid: wxContext.OPENID,
          createTime: new Date().getTime(),
          dynamicId: params.dynamicId
        }
      })
    }
  } else {
    endorseResult = await db.collection("dynamicEndorse").where({
      _openid: wxContext.OPENID,
      dynamicId: params.dynamicId
    }).remove()
  }
  return endorseResult
}

module.exports = dynamicEndorse