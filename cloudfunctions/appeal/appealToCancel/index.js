// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init()
const db = cloud.database()

// 取消点赞
const appealToCancel = async function (params) {
  const wxContext = cloud.getWXContext()

  try {

    let result = await db.collection('appealEndorse').where({
      _openid: wxContext.OPENID,
      appealId: params.appealId
    }).remove()

    return result

  } catch (error) {

    console.log('appealToCancel error' + error)

    return false

  }



}

module.exports = appealToCancel